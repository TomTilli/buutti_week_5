import express from "express";
// import router from "./api/router.js";

const app = express();
app.use(express.json());

// app.use("/route", router);

const port = 5005;

if(process.env.NODE_ENV !== "test"){
    app.listen(port, () => {
        console.log(`listening to port ${port}`);
    });
};

const books = [];
let bookId = 0;
const logList = [];

if(process.env.NODE_ENV === "test" || process.env.NODE_ENV == "dev"){
    // testausta varten
    books.push({id:1, name:"kirjannimi",author:"kirjailija", read:true});
    books.push({id:2, name:"Kirjannimi_2", author:"Kirjailija_2", read:false});
    bookId = 2;
};

const unknownEndpoint = (_req, res) => {
    res.status(404).send({error: "Endpoint can not be found"});
};

// heittää kyllä error viestejä rakennetutihin erroreihin kun rikkoo koodia,
//mutta en tiedä mitä erroria toimivalla koodilla ottaa kiinni
const errorHandler = (error, req, res, next) => {
    // console.log("täällä");
    console.error(error.message); //note to self. Tällä näkee mikä error kyseessä
    if(error.name === "ReferenceError") {
        res.status(400).send("An error has occured in the code");
    };
    next(error); //<- enkä täysin tiedä ymmärränkö tätä. Siirtääkö errorin "defaultErrorhandlerille" jos ei custom ota kiinni?
};

const getActualRequestDurationInMilliseconds = (start) => {
    const NS_PER_SEC = 1e9;
    const NS_TO_MS = 1e6;
    const diff = process.hrtime(start);
    return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};

//Loggaa tällä hetkellä vaa arrayhi
const logLogger = (req, res, next) => {
    const current_datetime = new Date();
    const formatted_datettime =
        current_datetime.getDate() + "-" +
        (current_datetime.getMonth() + 1) + "-" +
        current_datetime.getFullYear() + " " +
        current_datetime.getHours() + ":" +
        current_datetime.getMinutes() + ":" +
        current_datetime.getSeconds();
    const method = req.method;
    const url = req.url;
    const status = res.statusCode;
    const start= process.hrtime();
    const durationInMilliseconds = getActualRequestDurationInMilliseconds(start);
    const log = `[${formatted_datettime}] ${method}:${url} ${status} | ${durationInMilliseconds.toLocaleString()} ms`;
    console.log(log); 
    logList.push(log);
    next();
};
app.use("/api/v1/books", logLogger);

//kirjaa logilistan consoliin eikä käyttäjälle
app.get("/api/v1/logs", (req, res) => {
    logList.forEach(elem => console.log(elem));
    res.send("Logs found");
});

//List all books
app.get("/api/v1/books", (req, res) => {
    const registry = {};

    books.forEach((elem, index) => Object.assign(registry,{[index]:elem}));
    res.send(registry);
});

//Search for a book by its ID
app.get("/api/v1/books/:id",(req, res, next) => {
    const bookToFind = books.find(elem => (elem.id === Number(req.params.id)))
    if(books.indexOf(bookToFind) === -1){
        res.send("No entry was found with the given ID.");
    }else{
        res.send(bookToFind);
    };
});

//Create a new book
app.post("/api/v1/books", (req, res) => {
    bookId++;
    const name = req.body.name || "Empty";
    const author = req.body.author || "Empty";
    const read = req.body.read || false;
    const newBook = {id:bookId, name:name, author:author, read:read};
    books.push(newBook);
    res.send(`${req.body.name} added`);
});

// modify an existing entry by ID. Modiefied entries in res.body
app.put("/api/v1/books/:id", (req, res) => {
    
    const bookToModify = books.find(elem => elem.id === Number(req.params.id));
    
    if(books.indexOf(bookToModify) === -1){
        res.send("No entry was found with the given ID.");
    }else{
        if(req.body.name) bookToModify.name = req.body.name;
        if(req.body.author) bookToModify.author = req.body.author;
        if(req.body.read) bookToModify.read = req.body.read;

        res.send("modified entry");
    };
});

//delete an entry by its ID
app.delete("/api/v1/books/:id", (req, res) => {
    const bookToDelete = books.find(elem => elem.id === Number(req.params.id));

    if(books.indexOf(bookToDelete) === -1){
        res.send("No entry found with the given ID");
    }else{
        books.splice(books.indexOf(bookToDelete),1);

        res.send("Entry deleted");
        // res.send(bookToDelete);
    };
});

//UnknownEndpoint funktio täytyy ottaa käyttöön endpointtien jälkeen jos kaikki endpointit ohitetaan
app.use(unknownEndpoint);

//ErrorHandler has to be the last defined middleware
app.use(errorHandler);

export default app;