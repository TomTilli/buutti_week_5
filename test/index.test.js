import request from "supertest";
import app from "../index.js";

//kaikissa testeissä listassa on jo kaks entryä valmiiks
//async modulen "series" näyttäs ainaki auttavan asiaan jos haluaa testikoodia pois. Tai ehkä axios

describe("testing adding book entries", () => {
    it("returns 200 when an entrie is added with all params", (done) => {
        const query = {name:"nimi", author:"kirjailija", read:true};

        request(app)
            .post("/api/v1/books")
            .send(query)
            .expect(200, done);
    });
    // tyhmiä testejä kun ei kato tuloste enempää ku et menee läpi, mutta nyt ei ole aikaa selvittää enempää

    // it("returns 200 when a entire is added with missing params", (done) => {
    //     const query = {name:"nimi", author:"kirjailija", read:true};

    //     request(app)
    //         .post("/api/v1/books")
    //         .send(query)
    //         .expect(200, done);
    // });

    // it("returns 200 when a entire is added with wrong params", (done) => {
    //     const query = {name:"nimi", author:"kirjailija", read:true};

    //     request(app)
    //         .post("/api/v1/books")
    //         .send(query)
    //         .expect(200, done);
    // });

    // it("returns 200 when a entire is added with extra params", (done) => {
    //     const query = {name:"nimi", author:"kirjailija", read:true};

    //     request(app)
    //         .post("/api/v1/books")
    //         .send(query)
    //         .expect(200, done);
    // });
});

describe("testing listing book entries" , () => {
    it("returns 200 when listing all entries", (done) => {
        request(app)
            .get("/api/v1/books")
            .expect(200, done);
    });

    it("returns the correct book with id", (done) => {
        //tämä sitä varten jos lisäis eka entryn ennen etsimistä
        // const query = {name:"nimi", author:"kirjailija", read:true};
        // request(app)
        //     .post("/api/v1/books")
        //     .send(query, done);

        request(app)
            .get("/api/v1/books/1")
            .expect(200)
            .expect({id:1, name:"kirjannimi", author:"kirjailija",read:true}, done);
    });

    it("returns an error message when an Id is used that does not exist", (done) => {
        request(app)
            .get("/api/v1/books/6")
            .expect(200)
            .expect("No entry was found with the given ID.", done);
    });
});

describe("Testing modifying an entry", () => {
    const query = {name:"uusiKirja", read:false};

    it("returns 200 when modifying an entry", (done) => {
        request(app)
            .put("/api/v1/books/1")
            .send(query)
            .expect(200)
            .expect("modified entry", done);
    });
});

describe("Testing deleting an entry", () => {

    it("returns 200 when deleting an entry", (done) => {
        request(app)
            .delete("/api/v1/books/1")
            .expect(200)
            .expect("Entry deleted", done);
    });
});